# Todo

Things to do.

## Router
- Add 192.168.100.0/24 for modem
- IPv6
- Add a second container for redundancy
    - VRRP does what is needed on the LAN side
    - NAT state needs sync'd, possibly split to a new pair of containers
    - VRRP does _not_ do what we need on the WAN side:
        - we shouldn't "leak" VRRP info on the WAN interface, it should all be
          internal to aluminum
        - The address needs to be assigned via DHCP.
          This is probably not incompatible with the VRRP protocol, but it is
          not supported in most, if any implementations.

## DHCP
- Replace isc-dhcp with kea or something else

## DNS

### Recursive

- Create the zone
- DoT stub
- Update resolvers in other zones
- Update DNS server list in DHCP options

### Authoritative

- Create zone
- Add local stuff
- Enable DDNS

## Misc
- Better config management
    - Chef comes up a lot
- harden ssh
- `tmux-256color` terminfo

## Monitoring/maintenance
- Alert if something goes wrong
- Are the zones under spec'd?
- How to apply updates to the global zone? Managed zones?
- APU firmware updates


# To not do

These are the things that I considered and have (currently) decided against:

## Router
- Split NAT into its own zone.
    - Intent is to keep the border router to _only_ do border routing.
      The intent is partly for philosophical reasons and partly to just reduce
      the attack surface as much as possible.
      However, this effectively just adds extra hops to the routing, and doesn't
      actually gain anything.
- Add Shentel's network
    - Possibly without NAT when going to the 10.x.x.x
    - Possibly with a routing protocol
    - I haven't figured out what this would actually accomplish, other than
      potentially upset Shentel.
