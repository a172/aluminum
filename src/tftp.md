# TFTP

## Create a TFTP zone
`tftp.json`
```json
{{#include containers/tftp.json}}
```

## Install the TFTP server

From inside the zone, run:

```
pkgin update
pkgin upgrade
pkgin install tftp-hpa
```

## Setup the TFTP service

```bash
mkdir /tftpboot # directory to serve from
echo 'tftp dgram udp wait root /opt/local/sbin/in.tftpd in.tftpd -s /tftpboot' > /tmp/tftp.inetd
inetconv -i /tmp/tftp.inetd -o /tmp
svccfg import /tmp/tftp-udp.xml
```

