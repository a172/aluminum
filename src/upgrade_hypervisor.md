# Upgrading the Hypervisor

Up to date instructions for a variety of installs can be found
[here][instructions].

## Download latest SmartOS

1. mount the sdcard SmartOS partition to `/mnt`:
    ```
    foldcase /dev/dsk/c1t0d0s2 /mnt
    ```
1. Verify:
    ```
    # ls /mnt
    boot      platform
    # find /mnt/platform
    /mnt/platform
    /mnt/platform/root.password
    /mnt/platform/i86pc
    /mnt/platform/i86pc/kernel
    /mnt/platform/i86pc/kernel/amd64
    /mnt/platform/i86pc/kernel/amd64/unix
    /mnt/platform/i86pc/amd64
    /mnt/platform/i86pc/amd64/boot_archive.hash
    /mnt/platform/i86pc/amd64/boot_archive.manifest
    /mnt/platform/i86pc/amd64/boot_archive.gitstatus
    /mnt/platform/i86pc/amd64/boot_archive
    ```
1. [Download][download] the latest version of SmartOS
1. scp it to `/tmp` of the hypervisor
1. untar it to `/mnt`:
    ```
    [root@aluminum /mnt]# tar xf /usbkey/smartos-platform-20221229T182454Z.tgz
    ```
    Tar will warn about file permissions having changed.
    This is OK.
1. Swap the platform:
    ```bash
    mv /mnt/platform /mnt/platform-previous
    mv /mnt/platform-20221229t182454z /mnt/platform
    ```
1. Verify new version:
    ```
    $ ssh root@aluminum.waldrep.lan
    SmartOS (build: 20221229T182454Z)
    [root@aluminum ~]# cat /etc/issue
    
    
    
             *--+--*--*
             |\ |\ |\ |\
             | \| \| \| \     #####  ####   #  #####  ###   #   # TM
             +--*--+--*--*      #    #   #  #    #   #   #  ##  #
             |\ |\ |\ |\ |      #    ####   #    #   #   #  # # #
             | \| \| \| \|      #    #  #   #    #   #   #  #  ##
             *--+--+--+--+      #    #   #  #    #    ###   #   #
              \ |\ |\ |\ |
               \| \| \| \|     SmartOS
                *--+--*--*      20221229T182454Z
    
    
    [root@aluminum ~]#
    ```

[download]: https://wiki.smartos.org/download-smartos/
[instructions]: https://wiki.smartos.org/remotely-upgrading-a-usb-key-based-deployment/
