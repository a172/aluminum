# Creating zones

These steps are repeated often.
Unless otherwise stated, all zones are built on the most recent minimal image.

## Check if the desired image is already imported
```
imgadm list uuid=<uuid>
```

## Get the most recent image
- Update the images: `imgadm update`
- Find the suitable image: `imgadm avail name=~minimal-64 pub=~<current_year>`
- Import the image: `imagadm import <uuid>`

```
[root@aluminum ~]# imgadm update
[root@aluminum ~]# imgadm avail name=~minimal-64 pub=~2021
UUID                                  NAME            VERSION  OS       TYPE          PUB
800db35c-5408-11eb-9792-872f658e7911  minimal-64-lts  20.4.0   smartos  zone-dataset  2021-01-11
[root@aluminum ~]# imgadm import 800db35c-5408-11eb-9792-872f658e7911
Importing 800db35c-5408-11eb-9792-872f658e7911 (minimal-64-lts@20.4.0) from "https://images.joyent.com"
Gather image 800db35c-5408-11eb-9792-872f658e7911 ancestry
Must download and install 1 image (32.5 MiB)
Download 1 image    [====================>] 100%  32.52MB   5.21MB/s     6s
Downloaded image 800db35c-5408-11eb-9792-872f658e7911 (32.5 MiB)
...792-872f658e7911 [====================>] 100%  32.52MB   6.12MB/s     5s
Imported image 800db35c-5408-11eb-9792-872f658e7911 (minimal-64-lts@20.4.0)
```

## Create a zone
```
[root@aluminum ~]# vmadm create -f zone.json
Successfully created VM xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
```

## Validate a zone definition
```
vmadm validate create -f zone.json
```
