# Summary

- [Intro](./README.md)
- [Initial boot](./initial_boot.md)
- [Creating zones](./creating_zones.md)
- [Routing/NAT](./router.md)
- [DHCP](./dhcp.md)
- [DHCP (Kea)](./dhcp-kea.md)
- [DNS (recursive)](./dns-unbound.md)
- [DNS (authoritative)](./dns-bind.md)
- [TFTP](./tftp.md)
- [maintenance]()
    - [Upgrading the Hypervisor](upgrade_hypervisor.md)
- [Todo](./todo.md)
