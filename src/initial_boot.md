# Initial boot

## Goal / transition

The old system was a Linux Distro booting off of the SD card.
The end result will be having a SmartOS image on the SD card and the mSATA card
be the zpool.
For the transition, we will boot SmartOS off of the USB drive.

It may occasionally be helpful to temporarily ignore the ssh host keys while we
switch.
Obviously, this is usually not recommended, but since we are directly connected
to the device, we aren't worried about a MitM attack.
```bash
ssh \
  -o UserKnownHostsFile=/dev/null \
  -o StrictHostKeyChecking=no \
  root@192.168.114.254
```

## Setup boot media

Go [here][dl] and download the SmartOS USB Image.
Extract it and `dd` it to a USB drive.
You will need _at least_ a 2Gb drive.

## Boot 0

- Choose the internal network as the `admin` network.
  - I chose the middle NIC (`igb1`)
  - Note these rarely appear in order.
    Look at the link name and/or MAC address.
  - You can connect/disconnect interfaces and check the state to verify which
    interface is which.
- Statically assign the hypervisor's IP address (e.g., `192.168.114.254/24`)
- No default gateway
- Use CloudFare's DNS servers:
  - `1.1.1.1`
  - `1.0.0.1`
- Choose a reasonable local domain (e.g., `waldrep.lan`) for the default search
  domain.
- Default NTP is fine.
- Pick a hostname (e.g., `aluminum`)
- Make sure that the only storage devices connected are the boot device and the
  desired system storage.
- Use the default zpool layout
- Boot from `zones`
- Source SmartOS from `media`
- Generate a root passphrase.
  - 22 alphanumeric characters (including upper and lower case) gives 128 bits
    of entropy.
- Verify config and reboot

## Boot 1

### Add global zone ssh key
Taken from the [wiki][wiki_extra] and a [blog][blog_tweaks].

We'll add a bit of config that will install a file as the `authorized_keys`
file.
```
# grep authorized /usbkey/config
root_authorized_keys_file=authorized_keys
# cat /usbkey/config.inc/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJEZNYfTq/7UslOUBA43ef7VkgAVMYXHDvFOnHy0vXnc openpgp:0xC14AB15A
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHI0bfpvlr4RxmiEyBN+VMejWhuSAi92UYat1JskQarn openpgp:0xB72E2D0E
```

This didn't work until a reboot.
I'm not sure why `sysinfo -u` didn't work.

### Internet connectivity

Before we can setup any zones, we need to be able to get an image from Joyent's
servers.
Our actual router itself is going to be a zone, so we'll temporarily bring the
WAN connection into the global zone.
```bash
dladm create-vnic -l igb0 wan0
ifconfig wan0 plumb
ifconfig wan0 dhcp
```

[dl]: https://wiki.smartos.org/download-smartos/
[wiki_extra]: https://wiki.smartos.org/extra-configuration-options/
[blog_tweaks]: https://perkin.org/uk/posts/smartos-global-zone-tweaks.html
