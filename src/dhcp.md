# DHCP

## IPAM
Yes, I know.
A table in a markdown file is terrible IPAM, but for a single /24, it will work
for now.

| Host    | Address(es)       | Notes  |
|---------|-------------------|--------|
| boron   | `192.168.114.4`   | switch |
| carbon  | `192.168.114.5`   | AP VIP |
| dhcp clients | `192.168.114.10` - `.199` | |
| argon   | `192.168.114.210` | future file server |
| krypton | `192.168.114.211` | future file server |

## Create a DHCP zones

`dhcp-0.json`:
```json
{{#include containers/dhcp-0.json}}
```
`dhcp-1.json`:
```json
{{#include containers/dhcp-1.json}}
```

## Install the DHCP server

From inside the zone, run:
```
pkgin update
pkgin upgrade
pkgin install isc-dhcpd
```

Configure the ISC DHCP server in `/opt/local/etc/dhcp`:
```nix
{{#include config/dhcpd.conf}}
```

From [`dhcpd.leases(5)`](https://kb.isc.org/docs/isc-dhcp-44-manual-pages-dhcpdleases):
> When dhcpd is first installed, there is no lease database.
> However, dhcpd requires that a lease database be present before it will start.

It seems this is sometimes necessary to create this manually:
```bash
touch /var/db/isc-dhcp/dhcpd.leases
```

Start the service:
```
[root@dhcp ~]# svcadm enable isc-dhcpd
```
