# Routing/NAT

## Create the WAN vnic
First, create the `wan` tag:
```bash
nictagadm add wan 00:0d:b9:47:c8:98
```

This should add the following line to `/usbkey/config`:
```
wan_nic=00:0d:b9:47:c8:98
```

## Tear down the temporary connection

I ended up doing it with a reboot along the way.
There is probably a better way to do it.

## Create the container

`router.json`:
```json
{{#include containers/router.json}}
```

For some reason, this is insisting on creating the zone with nics `net0` and
`net1` instead of `lan0` and `wan0`.
Grab the current nics, push an update, and restart the zone:
```
[root@aluminum ~]# vmadm get $(vmadm list -o uuid -H alias=router) | json nics
[
  {
    "interface": "net0",
    "mac": "d2:86:ad:9a:47:b5",
    "nic_tag": "admin",
    "netmask": "255.255.255.0",
    "ip": "192.168.114.1",
    "ips": [
      "192.168.114.1/24"
    ],
    "allow_ip_spoofing": true,
    "primary": true
  },
  {
    "interface": "net1",
    "mac": "52:e2:0c:1f:2d:cc",
    "nic_tag": "wan",
    "ip": "dhcp",
    "ips": [
      "dhcp"
    ],
    "allow_ip_spoofing": true
  }
]
cat - << EOF > router-update.json
> {
>   "update_nics": [
>     {
>       "interface": "lan0",
>       "mac": "d2:86:ad:9a:47:b5"
>     },
>     {
>       "interface": "wan0",
>       "mac": "52:e2:0c:1f:2d:cc"
>     }
>   ]
> }
> EOF
[root@aluminum ~]# vmadm validate update joyent -f router-update.json
VALID 'update' payload for joyent brand VMs.
[root@aluminum ~]# vmadm update $(vmadm list -o uuid -H alias=router) < router-update.json
Successfully updated VM xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
[root@aluminum ~]# vmadm reboot $(vmadm list -o uuid -H alias=router)
Successfully completed reboot for VM xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
```

## Configure NAT
Log into the zone, either with `zlogin $(vmadm list -o uuid -H alias=router)` or
sshing directly to it.

Edit `/etc/ipf/ipnat.conf`:
```
map wan0 192.168.114.0/24 -> 0/32 portmap tcp/udp auto
map wan0 192.168.114.0/24 -> 0/32
rdr lan0 10.238.71.119/32 -> 192.168.114.1
```
The last line is only needed to be able to ping the router's external address
from the internal network.
That also should not be needed, as the routing itself should do that.
Also, hard coding the public IP, which is assigned through DHCP is a bad idea.
This part needs some work.

Turn on IPv4 forwarding and enable ipf.
```
routeadm -u -e ipv4-forwarding
svcadm enable ipfilter
```

And verify the services started:
```
[root@router ~]# svcs ipv4-forwarding ipfilter
STATE          STIME    FMRI
online          5:17:43 svc:/network/ipfilter:default
online          5:17:44 svc:/network/ipv4-forwarding:default
```
