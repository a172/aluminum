# DHCP (Kea)

ISC DHCP was discontinued in 2022, so we need something else.
Kea (also from ISC) is the official and common replacement.
Unfortunately, Kea is not in the SmartOS repositories.

My solution is to run a Docker container for Kea.
Running a Debian image and installing Kea there would also be reasonable.

# Getting docker images on SmartOS


# Resources

1. [Docker on SmartOS](https://www.gaige.net/docker-on-smartos.html)
