# DNS
![](charts/dns.svg)

## Create a DNS zone

`unbound-0.json`:
```json
{{ #include containers/unbound-0.json }}
```
`unbound-1.json`:
```json
{{ #include containers/unbound-1.json }}
```

## Setup temporary DNS resolution
```bash
echo nameserver 1.1.1.1 >> /etc/resolv.conf
```

## Install unbound

From inside the zone, run:
```
pkgin update
pkgin upgrade
pkgin install unbound
```

## Configure unbound

Copy `DigiCert_Global_Root_G2.pem` to `/opt/local/etc/unbound` in the container.
This can be found on the hypervisor at
`/zones/<uuid>/root/opt/local/etc/unbound`.

Edit `/opt/local/etc/unbound/unbound.conf`:

```
{{ #include config/unbound.conf }}
```

## Configure syslog
Edit `/etc/syslog.conf` to include:
```
daemon.debug    /var/log/unbound.log
```

Prep the file:
```bash
touch /var/log/unbound.log
```
Restart syslog:
```bash
svcadm restart system-log
```

## Remove temporary resolution
```bash
echo nameserver 127.0.0.1 > /etc/resolv.conf
```

## Start the service
```bash
svcadm enable unbound
```

## Configure other services
Definitely run through the validation before implementing this step.

### DHCP
Edit `dhcpd.conf`:
```nix
# Howth
subnet 192.168.114.0 netmask 255.255.255.0 {
  # ...
  option domain-name-servers 192.168.114.251, 192.168.114.250;
  # ...
}
```

### Hypervisor
Edit `/usbkey/config`:
```conf
dns_resolvers=192.168.114.251,192.168.114.250
```
Also edit `/etc/resolv.conf` or reboot.

### Zones
Any existing zones need to be updated to use the new resolvers.
`/usbkey/containers/update_resolvers.json`:
```json
{
  "resolvers": [
    "192.168.114.251",
    "192.168.114.250"
  ]
}
```
Update the desired zones:
```bash
for uuid in aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb; do
    vmadm update $uuid -f update_resolvers.json
done
```
Also update any saved json files.

## Validation
While troubleshooting or validating things, some extra logging may be helpful:
```
server:
        log-queries: yes
        log-replies: yes
        log-tag-queryreply: yes
        log-destaddr: yes
```

### basic functionality
From the unbound localhost:
```bash
dig example.com @::1
dig example.com @192.168.114.25x
```
From another host on the network:
```bash
dig example.com @192.168.114.25x
```

### chroot
This should be enabled by default, but it isn't a bad idea to verify it (though
I wish there were an easier way).
Edit `unbound.conf` to include:
```
server:
    logfile: /unbound.log
    use-syslog: no
```
Prep the log file:
```bash
cd /opt/local/etc/unbound
touch unbound.log
chown unbound:unbound unbound.log
```
Restart unbound with `svcadm restart unbound`.
There should be logs in `/opt/local/unbound/unbound.log`.

This isn't where we actually want logs to go, so now undo all that.

### logging
Watch a log come through by restarting the service:
```bash
tail -f /var/log/unbound.log
```
```bash
svcadm restart unbound
```

### TLS
Use [`snoop`](https://smartos.org/man/8/snoop) to do a packet capture while
triggering an upstream lookup.
From the zone, do `snoop -P -o test.pacp port 853 1.1.1.1,1.0.0.1`.

scp this off and evaluate it with wireshark to make sure TLS is happening.

## TODO
### Rest of the owl
Most of the flowchart still needs implemented:
- Add block lists
- Serve an HTTP page for blocked domains
- handle local domains
- redirect other DNS traffic

### DoQ
DNS-over-QUIC (DoQ) for upstream requests has performance improvements over DoT,
but it is not well supported, yet.
Unbound supports it for downstream clients, but not for upstream requests.

At the moment, Cloudflare does not support DoQ either, but `dns.adguard-dns.com`
does at:
- `94.140.14.14:853`
- `94.140.15.15:853`
