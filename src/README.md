# Intro
Let's build a home router on a PC Engine's APU and SmartOS.
I'm a daily driver of Linux, and I don't use other Unixes, so I'm probably going
to misstep.
A lot.

I like the idea of isolating the different processes.
Zones seems like a good solution to this.
I like the idea of knowing what the heck my system is doing.
DTrace seems like a good solution to this.

So we're gonna try using SmartOS as a router (and DHCP server, and DNS server,
and the million other things that a home router does).

Is it a good idea?
I'm about to find out.

## Overview

| IP address        | Planned Host | Status  |
|-------------------|--------------|---------|
| `192.168.114.254` | hypervisor   | running |
| `192.168.114.253` | dhcp-0       | running |
| `192.168.114.252` | dhcp-1       | running |
| `192.168.114.251` | unbound-0    | running |
| `192.168.114.250` | unbound-1    | running |
| `192.168.114.249` | bind-0       | unbuilt |
| `192.168.114.248` | bind-1       | unbuilt |
| `192.168.114.247` | tftp         | running |
| `192.168.114.1`   | router       | running |

## Useful links
- [Modifying an instance](https://docs.smartos.org/managing-instances-with-vmamd/)
- [SMF Service Quick Reference](https://docs.smartos.org/smf-quick-reference/)
